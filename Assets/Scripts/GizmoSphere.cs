﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmoSphere : MonoBehaviour {
    public Color ColorOfGizmo;
    public float radius;

    void OnDrawGizmos() {
        Gizmos.color = ColorOfGizmo;
        Gizmos.DrawSphere(transform.position, radius);
    }
}