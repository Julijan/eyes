﻿using UnityEngine;

public class SetToPosition : MonoBehaviour {
    public Transform Target;

    public Vector3 Offset;

    private void Update() {
        transform.position = Target.position + Offset;
    }
}