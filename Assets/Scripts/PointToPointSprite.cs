﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class PointToPointSprite : MonoBehaviour {
    public Transform startPosition;
    public Transform endPosition;
    public bool mirrorZ = true;
    public List<SpriteRenderer> spriteRenderer;
    public SpriteRenderer shadowSpriteRenderer;
    public List<Sprite> Sprites;

    private float width;

    private bool CanChangeSprite = true;

    public bool SpriteMustNotBeTheSame = false;

    public bool IgnoreRotation = false;

    public bool Scalex = true;

    private void Start() {
        width = spriteRenderer[0].bounds.size.x; // they should be all same size anyway

        shadowSpriteRenderer.shadowCastingMode = ShadowCastingMode.ShadowsOnly;
    }

    private void Update() {
        Strech(gameObject, startPosition.position, endPosition.position, mirrorZ);

        if (CanChangeSprite) {
            StartCoroutine(ChangeSprite());
        }
    }

    public IEnumerator ChangeSprite() {
        CanChangeSprite = false;
        int randomSpriteIndex;
        randomSpriteIndex = Random.Range(0, Sprites.Count);

        if (SpriteMustNotBeTheSame) {
            while (spriteRenderer[0].sprite == Sprites[randomSpriteIndex]) {
                randomSpriteIndex = Random.Range(0, Sprites.Count);
            }
        }


        foreach (var spriteRender in spriteRenderer) {
            spriteRender.sprite = Sprites[randomSpriteIndex];
        }

        yield return new WaitForSeconds(0.1f);
        CanChangeSprite = true;
    }

    public void Strech(GameObject _sprite, Vector3 _initialPosition, Vector3 _finalPosition, bool _mirrorZ) {
        Vector3 centerPos = (_initialPosition + _finalPosition) / 2f;
        _sprite.transform.position = centerPos;
        if (!IgnoreRotation) {
            Vector3 direction = _finalPosition - _initialPosition;
            direction = Vector3.Normalize(direction);
            _sprite.transform.right = direction;
            if (_mirrorZ) _sprite.transform.right *= -1f;
        }


        Vector3 scale = new Vector3(1, 1, 1);
        if (Scalex) {
            scale.x = Vector3.Distance(_initialPosition, _finalPosition) / width;
        }
        else {
            scale.y = Vector3.Distance(_initialPosition, _finalPosition) / width;
        }

        _sprite.transform.localScale = scale;
    }
}