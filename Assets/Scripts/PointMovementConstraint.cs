﻿using UnityEngine;

[ExecuteInEditMode]
public class PointMovementConstraint : MonoBehaviour {
    // an array of game objects which will have a
    // line drawn to in the Scene editor
    public GameObject[] GameObjects;
}