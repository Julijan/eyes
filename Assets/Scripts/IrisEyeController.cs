﻿using System;
using UnityEngine;
using UnityEngine.Rendering;

public class IrisEyeController : MonoBehaviour {
    public Transform PlayerTransform;
    public Transform IrisTransform;
    public Transform IrisLookDirectionTransform;
    public MeshRenderer IrisMeshRenderer;
    public MeshRenderer IrisMeshShadowRenderer;

    public bool DebugEnabled = false;

    public Sprite EyeLashFull;
    public Sprite EyeLashMiddle;

    public SpriteRenderer LeftLashSpriteRenderer;
    public SpriteRenderer RightLashSpriteRenderer;

    public SpriteRenderer LeftLashShadowRenderer;
    public SpriteRenderer RightLashShadowRenderer;

    private void Start() {
        LeftLashShadowRenderer.shadowCastingMode = ShadowCastingMode.ShadowsOnly;
        RightLashShadowRenderer.shadowCastingMode = ShadowCastingMode.ShadowsOnly;
        IrisMeshShadowRenderer.shadowCastingMode = ShadowCastingMode.ShadowsOnly;
    }

    private void Update() {
        Vector3 dir = (transform.position - PlayerTransform.position).normalized;
        Vector3 forward = -transform.forward;
        dir.y = 0; // ignore y axis
        forward.y = 0;
        float angle = Vector3.SignedAngle(forward, dir, Vector3.up);

        ChangeEyeLash(angle);

        float textureOffset1 = MyMathUtils.Linear(angle, -180, 180, 0.7f, -0.7f);

        dir = (transform.position - PlayerTransform.position).normalized;
        dir.x = 0; // ignore x axis
        forward = -transform.up;
        forward.x = 0;
        float IgnoreXAngle = Vector3.SignedAngle(forward, dir, Vector3.up);


        dir = (transform.position - PlayerTransform.position).normalized;
        dir.z = 0; // ignore x axis
        forward = -transform.up;
        forward.z = 0;
        float IgnoreZAngle = Vector3.SignedAngle(forward, dir, Vector3.up);

        if (transform.position.y > PlayerTransform.position.y) {
            if (IgnoreXAngle < IgnoreZAngle) {
                angle = IgnoreXAngle;
            }
            else {
                angle = IgnoreZAngle;
            }
        }
        else {
            if (IgnoreXAngle > IgnoreZAngle) {
                angle = IgnoreXAngle;
            }
            else {
                angle = IgnoreZAngle;
            }
        }

        float textureOffset2 = MyMathUtils.Linear(angle, -30, 210, +0.7f, -0.7f);

        IrisMeshRenderer.material.SetTextureOffset("_MainTex",
            new Vector2((float) Math.Round(textureOffset1, 1), (float) Math.Round(textureOffset2, 1)));


        IrisTransform.LookAt(PlayerTransform);
        // IrisLookDirectionTransform.LookAt(PlayerTransform);
    }

    private void ChangeEyeLash(float angle) {
        // order is important!

        if (angle > -40) {
            LeftLashSpriteRenderer.sprite = EyeLashFull;
        }
        else if (angle < -65) {
            LeftLashSpriteRenderer.sprite = null;
        }
        else if (angle < -40) {
            LeftLashSpriteRenderer.sprite = EyeLashMiddle;
        }


        if (angle < 40) {
            RightLashSpriteRenderer.sprite = EyeLashFull;
        }
        else if (angle > 65) {
            RightLashSpriteRenderer.sprite = null;
        }
        else if (angle > 40) {
            RightLashSpriteRenderer.sprite = EyeLashMiddle;
        }


        if (DebugEnabled) {
            Debug.Log(angle);
        }
    }

    void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawRay(IrisLookDirectionTransform.position, IrisLookDirectionTransform.forward * 50);
    }
}