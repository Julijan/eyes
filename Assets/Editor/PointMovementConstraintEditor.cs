﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PointMovementConstraint))]
public class PointMovementConstraintEditor : Editor {
    // draw lines between a chosen game object
    // and a selection of added game objects

    void OnSceneGUI() {
        // get the chosen game object
        PointMovementConstraint t = target as PointMovementConstraint;

        if (t == null || t.GameObjects == null)
            return;

        // grab the center of the parent
        Vector3 center = t.transform.position;

        // iterate over game objects added to the array...
        for (int i = 0; i < t.GameObjects.Length; i++) {
            // ... and draw a line between them
            if (t.GameObjects[i] != null) {
                if (Distance(t.transform, t.GameObjects[i].transform)) {
                    Handles.color = Color.red;
                }
                else {
                    Handles.color = Color.blue;
                }
            }

            Handles.DrawLine(center, t.GameObjects[i].transform.position);
        }
    }

    bool Distance(Transform transform, Transform target) {
        float distance = Vector3.Distance(transform.position, target.position);

        if (distance > 3) {
            transform.position = Vector3.MoveTowards(transform.position, target.position, 0.1f);
            return true;
        }

        if (distance < 3) {
            transform.position = Vector3.MoveTowards(transform.position, target.position, 0.1f);
            return true;
        }

        return false;
    }
}